package no.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Created by Snorre on 25.02.2015.
 */
@Controller
public class BaseController {

    @RequestMapping("/")
    public String welcome(ModelMap model) {

        return "index";

    }
}
