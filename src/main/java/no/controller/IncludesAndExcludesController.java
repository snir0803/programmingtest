package no.controller;

import no.service.IncludesAndExcludesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

/**
 * Created by Snorre on 25.02.2015.
 */

@Controller
public class IncludesAndExcludesController {

    @Autowired
    IncludesAndExcludesService includesAndExcludesService;

    @RequestMapping(method = RequestMethod.GET, value = "/includesandexcludes/[{includes}]/[{excludes}]", produces = "application/json")
    @ResponseBody
    public String findIntervals(@PathVariable String [] includes, @PathVariable String [] excludes) {

        ArrayList<IncludesAndExcludesService.Interval> list = includesAndExcludesService.findIntervals(includes, excludes);
        String s = "";

        for(IncludesAndExcludesService.Interval i: list) {
            s += i.start + "-" + i.stop + "\n";
        }

        return s;
    }
}
