package no.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Snorre on 25.02.2015.
 */

@Controller
public class FizzBuzzController {

    @RequestMapping(method = RequestMethod.GET, value = "/fizzbuzz/{start}/{stop}", produces = "application/json")
    @ResponseBody
    public String fizzBuzz(@PathVariable Integer start, @PathVariable Integer stop) {

        String fizzBuzz = "";
        for(int i = Math.max(start, 1); i <= stop; i++) {
            if((i%3) == 0) {
                fizzBuzz += "Fizz";
                System.out.print("Fizz");

            } if((i%5) == 0) {
                fizzBuzz += "Buzz";
                System.out.print("Buzz");

            } if((i%3) > 0 && (i%5) > 0){
                fizzBuzz += i;
                System.out.print(i);

            }
            fizzBuzz += "\n";
            System.out.println();
        }
        return fizzBuzz;

    }
}
