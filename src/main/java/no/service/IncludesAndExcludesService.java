package no.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Snorre on 25.02.2015.
 */

@Service
public class IncludesAndExcludesService {

    /**
     * Inner class for Intervals
     */
    public class Interval implements  Comparable<Interval>{

        public int start;
        public int stop;

        public Interval(int min, int max) {
            this.start = min;
            this.stop = max;
        }

        public int compareTo(Interval v) {

            if(this.start == v.start) {
                return this.stop - v.stop;
            }
            return this.start - v.start;
        }
    }

    /**
     * Method containing the algorithm for including and excluding intervals
     * @param in
     * @param ex
     * @return
     */
    public ArrayList<Interval> findIntervals(String [] in, String [] ex) {

        ArrayList<Interval> includes = getSortedIntervals(in);
        ArrayList<Interval> excludes = getSortedIntervals(ex);
        ArrayList<Interval> intervals = new ArrayList<Interval>();

        for(int i = 0; i < includes.size(); i++) {
            boolean hasBeenSplit = false;
            if(excludes.size() == 0) {
                return includes;
            }
            for(int e = 0; e < excludes.size(); e++) {

                //Example: in; 50-60, ex; 10-100
                if(excludes.get(e).start <= includes.get(i).start && excludes.get(e).stop >= includes.get(i).stop) {
                    includes.set(i, null);
                    break;
                }

                if(includes.get(i) == null) {
                    break;
                }

                //Example: in; 3-100, ex; 1-50
                if(includes.get(i).start >= excludes.get(e).start
                        && includes.get(i).stop > excludes.get(e).stop
                        && includes.get(i).start < excludes.get(e).stop) {
                    Interval v = new Interval(excludes.get(e).stop+1, includes.get(i).stop);
                    hasBeenSplit = checkOverlaps(intervals, v);

                //Example: in; 1-100, ex; 50-70
                } else if(includes.get(i).start < excludes.get(e).start
                        && includes.get(i).stop > excludes.get(e).stop) {
                    Interval v = new Interval(includes.get(i).start, excludes.get(e).start-1);
                    Interval v2 = new Interval(excludes.get(e).stop +1, includes.get(i).stop);
                    checkOverlaps(intervals, v);
                    hasBeenSplit = true;

                    if(intervals.size() > 0
                            && intervals.get(intervals.size()-1).stop >= v.start-1
                            && intervals.get(intervals.size()-1).stop >= v.stop) {
                        if(intervals.get(intervals.size()-1).stop <= v2.stop) {
                            Interval removed = intervals.remove(intervals.size()-1);
                            intervals.add(new Interval(removed.start, v.stop));
                            intervals.add(v2);
                        }

                    } else {
                        intervals.add(v2);
                    }

                //Example: in; 1-100, ex; 50-150
                } else if(includes.get(i).start < excludes.get(e).start
                        && includes.get(i).stop <= excludes.get(e).stop
                        && includes.get(i).stop >= excludes.get(e).start) {
                    Interval v = new Interval(includes.get(i).start, excludes.get(e).start - 1);
                    hasBeenSplit = checkOverlaps(intervals, v);

                   if(intervals.size() > 0
                            && !hasBeenSplit) {
                        Interval removed = intervals.remove(intervals.size() - 1);
                        intervals.add(new Interval(removed.start, v.stop));
                        hasBeenSplit = true;

                    }

                //No exlude intervals in the include intervals
                } else if((includes.get(i).start > excludes.get(e).stop && !hasBeenSplit)
                        || (includes.get(i).stop < excludes.get(e).start && !hasBeenSplit)) {
                    Interval v = new Interval(includes.get(i).start, includes.get(i).stop);
                    hasBeenSplit = checkOverlaps(intervals, v);
                }
            }
        }
        return intervals;
    }

    /**
     * Checks if the interval is overlapping with the previously stored interval
     * @param intervals
     * @param v
     * @return
     */
    private boolean checkOverlaps(ArrayList<Interval> intervals, Interval v) {

        if(intervals.size() > 0
                && intervals.get(intervals.size()-1).stop >= v.start-1
                && intervals.get(intervals.size()-1).stop <= v.stop) {
            Interval removed = intervals.remove(intervals.size() - 1);
            intervals.add(new Interval(removed.start, v.stop));
            return true;

        } else if(intervals.size() > 0
                && intervals.get(intervals.size()-1).stop >= v.start-1
                && intervals.get(intervals.size()-1).stop >= v.stop) {
            return false;

        } else {
            intervals.add(v);
            return true;
        }
    }

    /**
     * Sorts the includes and excludes
     * @param arr
     * @return
     */
    private ArrayList<Interval> getSortedIntervals(String [] arr) {

        Interval [] intervals = new Interval[arr.length];
        for(int i = 0; i < arr.length; i++) {
            int start = Integer.parseInt(arr[i].split("-")[0].replaceAll("\\s+",""));
            int stop = Integer.parseInt(arr[i].split("-")[1].replaceAll("\\s+",""));
            intervals[i] = new Interval(start, stop);
        }
        Arrays.sort(intervals);

        ArrayList<Interval> list = new ArrayList<Interval>();
        for(int i = 0; i < intervals.length; i++) {
            if(i > 0) {
                checkForInitialOverlaps(list, intervals[i]);
            } else {
                list.add(intervals[i]);
            }
        }

        return list;
    }

    /**
     * Merges includes -or excludes-intervals if overlapping
     * @param intervals
     * @param v
     */
    private void checkForInitialOverlaps(ArrayList<Interval> intervals, Interval v) {

        if(intervals.size() > 0
                && intervals.get(intervals.size()-1).stop >= v.start-1) {
            Interval remove = intervals.remove(intervals.size()-1);
            intervals.add(new Interval(remove.start, Math.max(remove.stop, v.stop)));

        } else {
            intervals.add(v);
        }
    }
}
